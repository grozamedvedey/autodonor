var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until,
    keys = webdriver.keys;
var chrome = webdriver.Capabilities.chrome();
chrome.set('chromeOptions', {'args': ['--incognito', '--test-type']});

var driver = new webdriver.Builder()
    .withCapabilities(chrome)
    .build();

// auth emex
driver.get('http://www.emex.ru');
driver.wait(until.elementLocated(By.css('.sitenav-logon > a > .sitenav-text')), 1000);
driver.findElement(By.css('.sitenav-logon > a > .sitenav-text')).click();
driver.wait(until.elementLocated(By.name('username')), 1000);
driver.findElement(By.name('username')).sendKeys('967937');
driver.findElement(By.name('password')).sendKeys('9a8233bf');
driver.findElement(By.css('.auth-btn-container > button')).click();
driver.wait(until.elementLocated(By.css('.sitenav-submenu-caption-text-id')), 1000);

//auth ixora-auto
driver.get('https://ixora-auto.ru');
driver.wait(until.elementLocated(By.className('Label Logon')), 1000);
driver.findElement(By.className('Label Logon')).click();
driver.wait(until.elementLocated(By.name('username')), 1000);
driver.findElement(By.name('username')).sendKeys('aarharov');
driver.findElement(By.name('password')).sendKeys('4bjjsq1x');
driver.findElement(By.css('.FieldSet > form > .Logon')).click();
driver.wait(until.elementLocated(By.linkText('выход')), 1000);
driver.findElement(By.className('region-btn change-current-region')).click();
driver.wait(until.elementLocated(By.linkText('Кировская область')), 1000);
driver.findElement(By.linkText('Кировская область')).click();

// auth Part-kom
// driver.get('https://part-kom.ru');
// driver.wait(until.elementLocated(By.className('pull-right close modal-closer')), 1000);
// driver.findElement(By.className('pull-right close modal-closer')).click();
// driver.findElement(By.name('txtLogin')).sendKeys('aarharov');
// driver.findElement(By.name('txtPassword')).sendKeys('4bjjsq1x');
// driver.findElement(By.css('#user-form > form > .btn')).click();

const express = require('express');
const cheerio = require('cheerio');

var app = express();

app.set('port', (process.env.PORT || 5000));

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.sendFile('index.html');
});

var makeResponse = function (serial, callback) {
  var result = {'values': []};
  var resultCounter = 0;
  var resultNumber = 2;

  //ixora-auto
  driver.get('https://ixora-auto.ru');
  driver.wait(until.elementLocated(By.css('.SearchInput > input')), 1000);
  driver.findElement(By.css('.SearchInput > input')).sendKeys(serial);
  driver.findElement(By.css('.SearchInput > .SearchBtn')).click();
  driver.wait(until.elementLocated(By.css('.SearchTableContainer')), 1000);

  driver.getPageSource().then(function(html){
    var $ = cheerio.load(html);
    var detailName = '';
    var detailMake = '';
    $('.SearchResultTable > tbody > tr').each( function (i, elem) {
      if ($(elem).hasClass('OwnBranchHide') ) return;
      var skipName = false;
      if($(elem).hasClass('DetailFirstRow')){
        detailInfo = $ ($(elem).children('.DetailName')[0]).text()
          .replace(/(\s\s)/g, '')
          .split(' ');
        detailMake = detailInfo[0];
        detailName = detailInfo[1];
        skipName = true;
      }
      var obj = {
        'Name': detailName,
        'Make': detailMake,
        'From': 'Ixora-auto'
      };
      $(elem).children('td').each( function(i, td) {
        var at = skipName? i-2 : i;
        if (at >= 0) {
          switch (at) {
            case 0:
              //count
              obj['Count'] = $(td).text();
              break;
            case 2:
              //price
              obj['Price'] = $(td).text();
              break;
            case 3:
              //deliver
              var dates = $(td).text().replace(/\s/g, '').split('-');
              obj['DeliverMin'] = dates[0];
              obj['DeliverMax'] = dates[1];
              break;
            case 4:
              //warehouse
              obj['Warehouse'] = $(td).text();
              break;
            default:
              break;
          }
        }
      });
      result.values.push(obj);
    });
    resultCounter += 1;
    if (resultCounter == resultNumber)
      callback(result);
  });

  //emex
  driver.get('https://emex.ru');
  driver.wait(until.elementLocated(By.css('#detail-num-input')), 1000);
  driver.findElement(By.css('#detail-num-input')).sendKeys(serial);
  driver.findElement(By.css('.search-button')).click();
  driver.wait(until.elementLocated(By.css('.heading')), 1000);
  driver.getPageSource().then(function(html) {

    var $ = cheerio.load(html);
    console.log('search emex');

    resultCounter += 1;
    if (resultCounter == resultNumber)
      callback(result);
  });
};

app.get('/:serial', function (req, res) {
  //res.json({'values':[example2, example4, example1, example3]});
  makeResponse(req.params.serial, function(obj){
    res.json(obj);
  });
  console.log('Search for ' + req.params.serial);
});

example1 = {
  'Name':'TR34025',
  'Make': 'AUTOWELT',
  'Price': 1400,
  'Count': 4,
  'DeliverMin': 1,
  'DeliverMax': 3,
  'Warehouse': 'Москва',
  'From': 'Part-kom'
}

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
