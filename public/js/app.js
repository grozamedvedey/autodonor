$(document).foundation()

var results = {};

var searchAnimationProps = {
  'margin-top': 0,
  'opacity': 0
};

var showProgressText = function() {
  $('#in_progress').fadeOut(400);
  $('#in_progress').fadeIn(200);
};

var hideProgressText = function(complete) {
  $('#in_progress').fadeOut(200, complete);
};

var compByPrice = function(a,b) {
  if (a.Price < b.Price) {
    return -1;
  }
  if (a.Price > b.Price) {
    return 1;
  }
  return 0;
};

var prepareResults = function() {
  results.detailsByName = {}
  // form 'details by name' object
  results.values.forEach ( function(item) {
    var offerIndex = 0;
    if (results.detailsByName[item.Make + ' ' + item.Name] != null) {
      i = results.detailsByName[item.Make + ' ' + item.Name];
      offerIndex = i.offers.length;
    }
    else {
      i = results.detailsByName[item.Make + ' ' + item.Name] = {};
      i.offers = [];
    }
    i.offers[offerIndex] = {};
    i.offers[offerIndex].Price = item.Price;
    i.offers[offerIndex].Count = item.Count;
    i.offers[offerIndex].DeliverMin = item.DeliverMin;
    i.offers[offerIndex].DeliverMax = item.DeliverMax;
    i.offers[offerIndex].Warehouse = item.Warehouse;
    i.offers[offerIndex].From = item.From;
  });

  // sort 'details by name'
  for (var i in results.detailsByName) {
    results.detailsByName[i].offers.sort(compByPrice);
  }
};

var showResults = function() {
  var resultString = "";
  for (var name in results.detailsByName) {
    id = 'row_' + name.replace(' ', '_');
    resultString += '<tr id="'+ id +'" onclick="toogleResults(this.id)">';
    var offer = results.detailsByName[name].offers[0];
    resultString += '<td>' + name +
                    '<span class="badge primary"style="margin-left: 20px"><b>' +
                    results.detailsByName[name].offers.length +
                    '</b></span></td>';
    resultString += '<td>' + offer.Count + '</td>';
    resultString += '<td>' + offer.Price + '</td>';
    resultString += '<td>' + offer.DeliverMin + ' - '+ offer.DeliverMax + '</td>';
    resultString += '<td>' + offer.Warehouse + '</td>';
    resultString += '<td>' + offer.From + '</td>';
    resultString += "</tr>";
  }


  //clear previous results
  $('#search_results > tbody').remove();
  $('#search_results').append(resultString);
  $('#search_results').fadeIn();
};

var toogleResults = function(id) {
  if ($('#'+id).attr('exp') == 'false' || $('#'+id).attr('exp') == null) {
    $('#'+id).attr('exp', 'true');
    expandResults(id);
  } else {
    $('#'+id).attr('exp', 'false');
    hideExpandedResults(id);
  }

};

var expandResults = function(id) {
  var name = id.replace('row_', '').replace('_', ' ');
  var resultString = "";
  for (var i = 1; i<results.detailsByName[name].offers.length; i++){
    resultString += '<tr hidden="True" class="add_' +
                    name.replace(' ', '_') +'">';
    var offer = results.detailsByName[name].offers[i];
    resultString += '<td></td>';
    resultString += '<td>' + offer.Count + '</td>';
    resultString += '<td>' + offer.Price + '</td>';
    resultString += '<td>' + offer.DeliverMin + ' - '+ offer.DeliverMax + '</td>';
    resultString += '<td>' + offer.Warehouse + '</td>';
    resultString += '<td>' + offer.From + '</td>';
    resultString += '</tr>';
  }
  selector_additional = '.add_'+ name.replace(' ', '_');
  $('#'+id).after(resultString);
  $(selector_additional).fadeIn(200);
};

var hideExpandedResults = function(id) {
  var name = id.replace('row_', '');
  $('.add_' + name).fadeOut(200, function(){$('.add_' + name).remove()} );
};

var onSearchSuccess = function (data, textStatus, jqXHR ){
  results = data;
  prepareResults();
  hideProgressText(showResults);
};

var fireSearch = function() {
  if ($('#search_field').val()) {
    $('#logo_movable').animate(searchAnimationProps, {'easing': 'easeOutCubic', 'duration': 600})
    $('#search_results').fadeOut(showProgressText);
    $.get(window.location.href + $('#search_field').val(), onSearchSuccess);
  }
};

$(document).ready( function() {
  searchAnimationProps['margin-top'] = -$('#logo_movable').height()+20;
  $('#search_btn').click(fireSearch);
  $('#search_field').keypress(function(e) {
    if(e.which == 13) {
        fireSearch();
    }
});
});
